package xyz.alfonsomora.nycschools.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import xyz.alfonsomora.nycschools.R;
import xyz.alfonsomora.nycschools.models.School;
import xyz.alfonsomora.nycschools.views.DetailsActivity;

public class SchoolsAdapter extends RecyclerView.Adapter<SchoolsAdapter.ViewHolder> {

    private List<School> schools;
    private Context context;

    void SchoolsAdapter(){
    }

    public void setData( List<School> schools ){
        this.schools = schools;
        notifyDataSetChanged();
    }

    public void addData( List<School> schools ){
        this.schools.addAll(schools);
        notifyDataSetChanged();
    }

    public void deleteData(){
        this.schools.clear();
        notifyDataSetChanged();
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new SchoolsAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.card_school, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        // Getting the school object to manage the data more easy
        School school = schools.get(position);

        // Showing the school details on cardView
        holder.text_name.setText(school.getSchoolName());
        holder.text_description.setText(school.getOverviewParagraph());
        holder.text_neighborhood.setText(school.getNeighborhood());

        // When the user press the card, we are going to show more details
        // on the DetailsActivity
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Going to details page
                Intent intent = new Intent(context, DetailsActivity.class);
                // Adding school to show in details page
                intent.putExtra("school", school);
                // Let's go!
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return schools.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        // TODO: we can show more details on the cardView, given more time

        TextView text_name;
        TextView text_description;
        TextView text_neighborhood;
        CardView cardView;


        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            text_name = itemView.findViewById(R.id.text_name);
            text_description = itemView.findViewById(R.id.text_description);
            text_neighborhood = itemView.findViewById(R.id.text_neighborhood);
            cardView = itemView.findViewById(R.id.card_view);
        }
    }
}
