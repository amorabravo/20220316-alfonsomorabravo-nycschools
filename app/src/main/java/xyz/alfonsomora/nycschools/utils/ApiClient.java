package xyz.alfonsomora.nycschools.utils;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import xyz.alfonsomora.nycschools.services.SatService;
import xyz.alfonsomora.nycschools.services.SchoolService;

public class ApiClient {

    private static Retrofit getRetrofit(){

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://data.cityofnewyork.us/resource/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        return retrofit;
    }

    public static SchoolService getSchoolService(){
        SchoolService schoolService = getRetrofit().create(SchoolService.class);
        return schoolService;
    }

    public static SatService getSatService(){
        SatService satService = getRetrofit().create(SatService.class);
        return satService;
    }
}
