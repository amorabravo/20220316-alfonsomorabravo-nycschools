package xyz.alfonsomora.nycschools.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class School implements Serializable {

    @SerializedName("dbn")
    private String dbn;

    @SerializedName("school_name")
    private String schoolName;

    @SerializedName("boro")
    private String boro;

    @SerializedName("overview_paragraph")
    private String overviewParagraph;

    @SerializedName("school_10th_seats")
    private int school10thSeats;

    @SerializedName("academicopportunities1")
    private String academicOpportunities1;

    @SerializedName("academicopportunities2")
    private String academicOpportunities2;

    @SerializedName("ell_programs")
    private String ellPrograms;

    @SerializedName("neighborhood")
    private String neighborhood;

    @SerializedName("building_code")
    private String buildingCode;

    @SerializedName("location")
    private String location;

    @SerializedName("phone_number")
    private String phoneNumber;

    @SerializedName("fax_number")
    private String faxNumber;

    @SerializedName("school_email")
    private String schoolEmail;

    @SerializedName("website")
    private String website;

    @SerializedName("subway")
    private String subway;

    @SerializedName("bus")
    private String bus;


    /***
     *
     * Getters and Setters
     */

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getBoro() {
        return boro;
    }

    public void setBoro(String boro) {
        this.boro = boro;
    }

    public String getOverviewParagraph() {
        return overviewParagraph;
    }

    public void setOverviewParagraph(String overviewParagraph) {
        this.overviewParagraph = overviewParagraph;
    }

    public int getSchool10thSeats() {
        return school10thSeats;
    }

    public void setSchool10thSeats(int school10thSeats) {
        this.school10thSeats = school10thSeats;
    }

    public String getAcademicOpportunities1() {
        return academicOpportunities1;
    }

    public void setAcademicOpportunities1(String academicOpportunities1) {
        this.academicOpportunities1 = academicOpportunities1;
    }

    public String getAcademicOpportunities2() {
        return academicOpportunities2;
    }

    public void setAcademicOpportunities2(String academicOpportunities2) {
        this.academicOpportunities2 = academicOpportunities2;
    }

    public String getEllPrograms() {
        return ellPrograms;
    }

    public void setEllPrograms(String ellPrograms) {
        this.ellPrograms = ellPrograms;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getBuildingCode() {
        return buildingCode;
    }

    public void setBuildingCode(String buildingCode) {
        this.buildingCode = buildingCode;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getSchoolEmail() {
        return schoolEmail;
    }

    public void setSchoolEmail(String schoolEmail) {
        this.schoolEmail = schoolEmail;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getSubway() {
        return subway;
    }

    public void setSubway(String subway) {
        this.subway = subway;
    }

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }


    /****
     *
     * Overriding toString method to get all school in only 1 line
     */

    @Override
    public String toString() {
        return "School{" +
                "dbn='" + dbn + '\'' +
                ", schoolName='" + schoolName + '\'' +
                ", boro='" + boro + '\'' +
                ", overviewParagraph='" + overviewParagraph + '\'' +
                ", school10thSeats=" + school10thSeats +
                ", academicOpportunities1='" + academicOpportunities1 + '\'' +
                ", academicOpportunities2='" + academicOpportunities2 + '\'' +
                ", ellPrograms='" + ellPrograms + '\'' +
                ", neighborhood='" + neighborhood + '\'' +
                ", buildingCode='" + buildingCode + '\'' +
                ", location='" + location + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", faxNumber='" + faxNumber + '\'' +
                ", schoolEmail='" + schoolEmail + '\'' +
                ", website='" + website + '\'' +
                ", subway='" + subway + '\'' +
                ", bus='" + bus + '\'' +
                '}';
    }
}
