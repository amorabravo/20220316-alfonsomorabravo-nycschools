package xyz.alfonsomora.nycschools.views;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.alfonsomora.nycschools.R;
import xyz.alfonsomora.nycschools.models.Sat;
import xyz.alfonsomora.nycschools.models.School;
import xyz.alfonsomora.nycschools.utils.ApiClient;

public class DetailsActivity extends AppCompatActivity {

    TextView text_name;
    TextView text_description;
    TextView text_writing_score;
    TextView text_reading_score;
    TextView text_math_score;
    TextView text_phone_number;
    TextView text_neighborhood;
    TextView text_address;
    TextView text_email;
    LinearLayout linearLayout;

    School school;
    Sat sat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        initializingComponents();

        // Getting the school to show all details
        Intent intent = getIntent();
        school = (School)intent.getSerializableExtra("school");

        // Updating the view with school sata
        showingData();

        // Requesting SAT score Data
        requestSATData();
    }

    /**
     * Initializing all components, this is a separated method to
     * maintain the OnCreate method clean and simple
     *
     * */
    private void initializingComponents() {
        // Showing back button
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        text_name = findViewById(R.id.text_name);
        text_description = findViewById(R.id.text_description);
        text_writing_score = findViewById(R.id.text_writing_score);
        text_reading_score = findViewById(R.id.text_reading_score);
        text_math_score = findViewById(R.id.text_math_score);
        text_neighborhood = findViewById(R.id.text_neighborhood);
        text_address = findViewById(R.id.text_address);
        text_phone_number = findViewById(R.id.text_phone_number);
        text_email = findViewById(R.id.text_email);
        linearLayout = findViewById(R.id.linear_sat_data);
    }

    /**
     * This method request SAT scores from the server
     *
     * */

    private void requestSATData() {
        Call<List<Sat>> satRequest = ApiClient.getSatService().getSat(school.getDbn());

        satRequest.enqueue(new Callback<List<Sat>>() {
            @Override
            public void onResponse(@NotNull Call<List<Sat>> call, @NotNull Response<List<Sat>> response) {

                // Verifying if the response is successful
                if (response.isSuccessful()){
                    assert response.body() != null;
                    // Verifying if the request contains SAT details
                    if (response.body().size() > 0){
                        Log.e("SUCCESS", response.body().toString());
                        sat = response.body().get(0);
                        setSATData();
                    }

                }
            }

            @Override
            public void onFailure(Call<List<Sat>> call, Throwable t) {
                Log.e("FAILURE SAT", t.getLocalizedMessage());
            }
        });

    }


    /**
     * If there are SAT information to this school
     * we'll show the linearLayout and set the scores
     * */
    private void setSATData() {
        linearLayout.setVisibility(View.VISIBLE);

        // SAT data
        String writing = "Writing\n" + sat.getSatWritingAvgScore();
        String reading = "Reading\n" + sat.getSatCriticalReadingAvgScore();
        String math = "Math\n" + sat.getSatWritingAvgScore();


        text_writing_score.setText(writing);
        text_reading_score.setText(reading);
        text_math_score.setText(math);
    }


    /**
     *
     * Updating the view with the school information
     */
    private void showingData() {

        // Basic information about the school
        text_name.setText(school.getSchoolName());
        text_description.setText(school.getOverviewParagraph());
        text_neighborhood.setText(school.getNeighborhood());
        text_address.setText(school.getLocation());
        text_phone_number.setText(school.getPhoneNumber());
        text_email.setText(school.getSchoolEmail());

        // TODO: If we have more time we can show more information
        // and include more functionalities like, google maps, etc...

        // Dialing phone number to call the school
        if (!school.getPhoneNumber().isEmpty()){
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + school.getPhoneNumber()));

            text_phone_number.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(intent);
                }
            });
        }

        // Opening a email client to send a email to school
        if (!school.getSchoolEmail().isEmpty()){
            composeEmail(school.getSchoolEmail());
        }

    }

    public void composeEmail(String address) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);

        // only email apps should handle this
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, address);

        text_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intent);
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item){
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}