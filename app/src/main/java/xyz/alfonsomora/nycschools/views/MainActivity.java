package xyz.alfonsomora.nycschools.views;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.alfonsomora.nycschools.R;
import xyz.alfonsomora.nycschools.adapters.SchoolsAdapter;
import xyz.alfonsomora.nycschools.models.School;
import xyz.alfonsomora.nycschools.utils.ApiClient;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    SchoolsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        // Setting de adapter to show all schools on the recyclerView
        recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SchoolsAdapter();

        // Getting the firsts 10 schools from the server
        getSchools(10,0);


    }

    /**
     *
     * This method show the first *limit* schools in the API
     * you can get more schools modifying the limit of items
     *
     */
    private void getSchools(int limit, int offset ) {
        Call<List<School>> schoolRequest = ApiClient.getSchoolService().getSchoolsPaginate(limit, offset);

        // TODO: We have to paginate to show more schools when the
        // scroll get all the way down, we can do modifying the offset to the next schools

        schoolRequest.enqueue(new Callback<List<School>>() {
            @Override
            public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                if (response.isSuccessful()){
                    Log.e("SUCCESS", response.body().get(0).getSchoolName());

                    List<School> schools =  response.body();
                    adapter.setData( schools );
                    recyclerView.setAdapter(adapter);

                }
            }

            @Override
            public void onFailure(Call<List<School>> call, Throwable t) {
                Log.e("FAILURE", String.valueOf(t.getLocalizedMessage()));
            }
        });
    }


}