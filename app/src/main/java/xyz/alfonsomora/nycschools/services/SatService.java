package xyz.alfonsomora.nycschools.services;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import xyz.alfonsomora.nycschools.models.Sat;

public interface SatService {

    final static String QUERY = "f9bf-2cp4.json";

    @GET(QUERY)
    Call<List<Sat>> getAllSats();

    @GET(QUERY)
    Call<List<Sat>> getSatsPaginate(
            @Query("$limit") int limit,
            @Query("$offset") int offset
    );

    @GET(QUERY)
    Call<List<Sat>> getSat(
            @Query("dbn") String dbn
    );
}
