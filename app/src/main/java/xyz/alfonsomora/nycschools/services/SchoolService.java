package xyz.alfonsomora.nycschools.services;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import xyz.alfonsomora.nycschools.models.School;

public interface SchoolService {

    @GET("s3k6-pzi2.json")
    Call<List<School>> getAllSchools();

    @GET("s3k6-pzi2.json")
    Call<List<School>> getSchoolsPaginate(
            @Query("$limit") int limit,
            @Query("$offset") int offset
    );

    @GET("s3k6-pzi2.json")
    Call<List<School>> getSchool(
            @Query("dbn") String dbn
    );


}
